﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculator
{
    public partial class Form1 : Form
    {
       Button[,] btns = new Button[3, 3];
        string MS;
      string MR;
         string operatorr;
         double x, y, z;
        BigNumber x1, y1, z1;
         int state;
        int k = 1;
        public  Form1()
        {
            InitializeComponent();

            state = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    btns[i, j] = new Button();
                    btns[i, j].Location = new Point(5 + j * 40, 160 + i * 40);
                    btns[i, j].Size = new Size(33, 33);
                    btns[i, j].Text = k.ToString();
                    k++;
                    btns[i, j].Click += new System.EventHandler(btn_click);
                    this.Controls.Add(btns[i, j]);
                }
            }
        }        
        private void btn_click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            if (state == 0 || state == 2)
            {
               
                    textBox1.Text += b.Text;
                
            }
            else if (state == 1)
            {
                
                textBox1.Text = b.Text;
                state = 2;
            }
            else if (state == 3)
            {
                
                textBox1.Text = b.Text;
                state = 0;
            }


        }
        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text += button1.Text;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text  += button7.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            operations("/");
        }
        public void operations(string name)
        {
            if (name == "+" || name == "-" || name == "/" || name == "*")
            {
                if (textBox1.Text == "") 
                    return;
                state = 1;
                x = double.Parse(textBox1.Text);
                operatorr = name;
            }
            else if (name == "=")
            {
                if (textBox1.Text == "")
                    return;
                if (state == 3)
                {

                    x = double.Parse(textBox1.Text);
                }
                else
                {

                    y = double.Parse(textBox1.Text);
                }
                state = 3;
                switch (operatorr)
                {
                    case "+":
                        x1 = new BigNumber(x.ToString());
                        y1 = new BigNumber(y.ToString());
                        z1 = x1 + y1;
                        break;
                    case "-":
                        z = x - y;
                        break;
                    case "/":
                        if (y == 0)
                        {
                            MessageBox.Show("error");
                            state = 0;
                            textBox1.Clear();
                            return;
                        }
                        else
                            z = x / y;
                        break;
                    case "*":
                        z = x * y;
                        break;
                }
                if (operatorr == "+")
                {
                    textBox1.Text = z1.ToString();
                }
                else
                    textBox1.Text = z.ToString();
            }

        }
        private void button4_Click(object sender, EventArgs e)
        {
           
           operations("+");
           

        }

        private void button6_Click(object sender, EventArgs e)
        {
            operations("=");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            operations("*");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            operations("-");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            state = 0;
            textBox1.Clear();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                return;
            string s = textBox1.Text;
            string t = "";
            for (int i = 0; i < s.Length-1; i++)
            {
                t += s[i];
            }
            textBox1.Text = t;

        }

        private void button11_Click(object sender, EventArgs e)
        {
            MS = textBox1.Text;
            label1.Text = "M";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (MS == "")
                return;
            double a = double.Parse(MS);
            double b = double.Parse(textBox1.Text);
            double c = a + b;
            MR = c.ToString();
            MS = MR;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (MS == "") return;
            double a = double.Parse(MS);
            double b = double.Parse(textBox1.Text);
            double c = a - b;
            MR = c.ToString();
            MS = MR;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            MS = "";
            MR = "";
            label1.Text = "";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (MS == "") 
                return;
            textBox1.Text = MR;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {

        }
    }
}
