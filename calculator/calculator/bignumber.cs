﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    public class BigNumber{
        public int[] a;
        public int sz;
        
        public BigNumber(string s)
        {
            a = new int[1000];
            sz = s.Length;
            for (int i=s.Length-1, j=0;i>=0;i--,j++){
                a[j] = s[i] -'0';
            }
        }
       
        public static BigNumber operator +(BigNumber b, BigNumber c)
        {

            for (int i = 0; i < Math.Max(b.sz, c.sz); i++)
            {

                
                b.a[i] = b.a[i] + c.a[i];
                b.a[i + 1] += b.a[i] / 10;
                b.a[i] %= 10;

                if (b.a[b.sz - 1] == 0)
                {
                    b.sz--;
                }
                else if (b.a[i] > 0)
                {
                    b.sz++;
                }
            }
      

            
            
            return b;
        }
        public override string ToString()
        {
            string s = "";
            for (int i = sz - 1; i >= 0; i--)
            {
                s += a[i].ToString();
            }
            return s;
        }

    }
    
    
}
